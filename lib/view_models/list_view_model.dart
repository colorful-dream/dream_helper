import 'package:dream_helper/dream_helper.dart';
import 'package:dream_helper/view_models/view_model.dart';
import 'package:flutter/cupertino.dart';

typedef RequestData<T> = Future<DataStatus> Function(bool, int, List<T>);

typedef IndexedItemBuilder<T> = Widget Function(BuildContext, List<T>, int);

abstract class ListViewModel<T> extends ViewModel {
  ListViewModel({required Network network}) : super(network: network);

  /// items
  List<T> items = [];

  /// 当前页 从第一页开始
  int page = 0;

  /// 数据状态
  DataStatus dataStatus = DataStatus.none;
}
