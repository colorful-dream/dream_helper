import 'package:package_info/package_info.dart';

class AppInfo {
  /// static [_shared]
  static AppInfo _shared = AppInfo._internal();

  String _version = '';

  Future<String> get version async {
    if (_version.isEmpty) {
      var result = await PackageInfo.fromPlatform();
      _version = result.version;
    }
    return _version;
  }

  /// private named construct
  AppInfo._internal();

  /// factory
  factory AppInfo() => _shared;

  /// [shared]
  static AppInfo get shared => _shared;
}
