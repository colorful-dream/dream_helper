import 'package:dream_helper/dream_helper.dart';
import 'package:flutter/cupertino.dart';

abstract class ViewModel extends ChangeNotifier {
  /// 网络请求
  Network _network;

  /// construct
  ViewModel({required Network network}) : this._network = network;

  /// getter [_network]
  Network get network => _network;

  /// 默认无状态
  RequestStatus _requestStatus = RequestStatus.none;

  /// getter [_requestStatus]
  RequestStatus get requestStatus  => _requestStatus;

  /// 数据状态
  DataStatus _dataStatus = DataStatus.none;

  /// getter [_dataStatus]
  DataStatus get dataStatus => _dataStatus;

  /// 取消 原因 [reason]
  void cancel({dynamic reason}) {
    _network.cancel(reason: reason);
  }

  /// 更新状态
  void changeStatus(RequestStatus requestStatus, DataStatus dataStatus) {
    _requestStatus = requestStatus;
    _dataStatus = dataStatus;
    notifyListeners();
  }
}
