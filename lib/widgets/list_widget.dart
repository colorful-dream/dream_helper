import 'package:dream_helper/dream_helper.dart';
import 'package:dream_helper/view_models/list_view_model.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyrefresh/easy_refresh.dart';

class ListViewModel<T> {
  /// items
  List<T> items = [];

  /// 当前页
  int current = 0;

  /// 数据状态
  DataStatus dataStatus;

  /// 请求状态
  RequestStatus requestStatus;

  /// 是否需要footer
  bool footer;

  ///
  bool get isLoading => requestStatus == RequestStatus.loading;

  int get count => items.length;

  ListViewModel(
      {this.dataStatus = DataStatus.none,
      this.requestStatus = RequestStatus.none,
      this.footer = false});
}

class ListWidget<T> extends StatefulWidget {
  ListWidget.single({
    Key? key,
    this.appBarWidgetBuilder,
    this.refresh,
    required this.indexedWidgetBuilder,
    this.scrollController,
    this.controller,
    this.viewModel,
    int pageSize = 30,
  })  : this.pageSize = pageSize > 0 ? pageSize : 30,
        super(key: key);

  /// header
  final RequestData<T>? refresh;

  /// Appbar
  final WidgetBuilder? appBarWidgetBuilder;

  /// EasyRefreshController to load or refresh
  final EasyRefreshController? controller;

  /// ScrollController
  final ScrollController? scrollController;

  final ListViewModel<T>? viewModel;

  final IndexedWidgetBuilder indexedWidgetBuilder;

  final int pageSize;

  @override
  _ListWidgetState createState() => _ListWidgetState<T>();
}

class _ListWidgetState<T> extends State<ListWidget<T>> {
  late ListViewModel<T> viewModel;

  late dynamic error;

  @override
  void initState() {
    super.initState();
    viewModel = widget.viewModel ?? ListViewModel<T>();
  }

  @override
  Widget build(BuildContext context) {
    return EasyRefresh.builder(
      onRefresh: onRefreshCallback,
      onLoad: onLoadCallback,
      controller: widget.controller,
      scrollController: widget.scrollController,
      builder: (context, physics, header, footer) {
        return CustomScrollView(
          physics: physics,
          slivers: [
            if (widget.appBarWidgetBuilder != null)
              widget.appBarWidgetBuilder!(context),
            if (widget.refresh != null) header!,
            SliverList(
              delegate: SliverChildBuilderDelegate(
                widget.indexedWidgetBuilder,
                childCount: viewModel.count,
              ),
            ),
            if (viewModel.footer && widget.refresh != null) footer!,
          ],
        );
      },
    );
  }

  void update() {
    setState(() {});
  }

  Future<void> onRefreshCallback() async {
    if (viewModel.isLoading) return;
    viewModel.requestStatus = RequestStatus.loading;
    viewModel.dataStatus =
        viewModel.footer ? DataStatus.noMore : DataStatus.none;
    update();
    try {
      var _items = <T>[];
      viewModel.dataStatus =
          await widget.refresh!(true, viewModel.current + 1, _items);
      viewModel.items = _items;
      viewModel.current = 1;
    } catch (e) {
      viewModel.dataStatus = DataStatus.error;
    } finally {
      viewModel.requestStatus = RequestStatus.none;
      if (mounted) update();
    }
  }

  Future<void> onLoadCallback() async {
    if (viewModel.isLoading) return;
    viewModel.requestStatus = RequestStatus.loading;
    viewModel.dataStatus =
        viewModel.footer ? DataStatus.noMore : DataStatus.none;
    update();
    try {
      viewModel.dataStatus =
          await widget.refresh!(true, viewModel.current + 1, viewModel.items);
      viewModel.current++;
    } catch (e) {
      viewModel.dataStatus = DataStatus.error;
    } finally {
      viewModel.requestStatus = RequestStatus.none;
      if (mounted) update();
    }
  }
}
