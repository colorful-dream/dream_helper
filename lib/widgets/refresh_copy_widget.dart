import 'package:dream_helper/dream_helper.dart';
import 'package:flutter/material.dart';

import '../network_service/request_helper.dart';
// import '';

typedef RetrieveDataCallback<T> = Future<DataStatus> Function(
    int page, List<T> list, bool refresh);

typedef ItemBuilder<T> = Widget Function(BuildContext context, T item);

typedef IndexedItemBuilder<T> = Widget Function(
    BuildContext context, List<T> list, int index);

class RefreshCopyWidget<T> extends StatefulWidget {
  const RefreshCopyWidget({
    Key? key,
    this.pageSize = 30,
    required this.onRetrieveData,
    required this.itemBuilder,
    this.initLoadingBuilder,
    this.loadingBuilder,
    this.separatorBuilder,
    this.headerBuilder,
    this.loadMoreErrorViewBuilder,
    this.emptyBuilder,
    this.noMoreViewBuilder,
    this.initFailBuilder,
    this.scrollController,
    this.initState,
    this.physics,
    this.sliver = true,
  }) : super(key: key);

  /// Data count per request
  final int pageSize;

  /// Retrieve data callback
  final RetrieveDataCallback<T> onRetrieveData;

  /// Loading indicator for the first page.
  final WidgetBuilder? initLoadingBuilder;

  /// Loading indicator for the first page.
  final WidgetBuilder? loadingBuilder;

  /// List item builder
  final IndexedItemBuilder<T> itemBuilder;

  /// List item separator builder
  final IndexedItemBuilder<T>? separatorBuilder;

  /// List header builder
  final ItemBuilder<List<T>>? headerBuilder;

  /// When request failed, build Error View
  final ItemBuilder? loadMoreErrorViewBuilder;

  /// Placeholder for no data.
  /// `refresh` indicates pull-refresh action.
  final Widget Function(VoidCallback refresh, BuildContext context)?
      emptyBuilder;
  final ItemBuilder<List<T>>? noMoreViewBuilder;
  final Widget Function(
          VoidCallback refresh, dynamic error, BuildContext context)?
      initFailBuilder;
  final ScrollController? scrollController;
  final RequestHelper? initState;
  final ScrollPhysics? physics;
  final bool sliver;

  @override
  _RefreshCopyWidgetState<T> createState() => _RefreshCopyWidgetState<T>();
}

class _RefreshCopyWidgetState<T> extends State<RefreshCopyWidget<T>> {
  dynamic error;
  late RequestHelper<T> state;
  dynamic initError;

  bool get _hasHeader => widget.headerBuilder != null;

  @override
  void initState() {
    super.initState();
    state = RequestHelper<T>();
  }

  loadMore() async {
    if (state.isLoading) return;
    state.requestStatus = RequestStatus.loading;
    try {
      var dataStatus = await widget.onRetrieveData(
          state.currentPage + 1, state.items, false);
      state.dataStatus = dataStatus;
      error = null;
      ++state.currentPage;
    } catch (e) {
      error = e;
      state.dataStatus = DataStatus.error;
    } finally {
      if (mounted) {
        update();
      }
    }
  }

  void update() {
    setState(() {});
  }

  Future<void> refresh(bool pullDown) async {
    if (state.isLoading) return;
    state.requestStatus = RequestStatus.loading;
    if (pullDown) {
      state.dataStatus = DataStatus.normal;
    }
    error = null;
    update();
    try {
      var _items = <T>[];
      var dataStatus = await widget.onRetrieveData(1, _items, pullDown);
      state.dataStatus = dataStatus;
      state.items = _items;
      state.currentPage = 1;
    } catch (e) {
      initError = e;
    } finally {
      if (mounted) {
        update();
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    if (initError != null) {
      if (widget.initFailBuilder != null) {
        return widget.initFailBuilder!(() {
          initError = null;
          refresh(false);
        }, initError, context);
      } else {
        return _buildInitFailedView();
      }
    }
    if (state.items.isEmpty) {
      return widget.sliver
          ? SliverFillRemaining(child: _buildInitLoadingOrErrorView(context))
          : _buildInitLoadingOrErrorView(context);
    }
    return widget.sliver ? _buildSliver() : _build();
  }

  Widget _buildInitLoadingOrErrorView(context) {
    if (state.isNoMore) {
      if (widget.emptyBuilder != null) {
        return widget.emptyBuilder!(() => refresh(false), context);
      } else {
        return _buildEmptyView(context);
      }
    } else {
      if (widget.initLoadingBuilder != null) {
        return widget.initLoadingBuilder!(context);
      } else {
        return Center(
          child: SizedBox(
            width: 22,
            height: 22,
            child: CircularProgressIndicator(strokeWidth: 2),
          ),
        );
      }
    }
  }

  Widget _buildSliver() {
    return SliverList(
      delegate: SliverChildBuilderDelegate(
        _itemBuilder,
        childCount: state.items.length + 1,
      ),
    );
  }

  Widget _buildInitFailedView() {
    return Material(
      child: InkWell(
        child: Center(
          child: Text("$initError"),
        ),
        onTap: () {
          initError = null;
          refresh(false);
        },
      ),
    );
  }

  // ListView.builder(
  // physics: widget.physics,
  // controller: widget.scrollController,
  // itemCount: state.items.length + (_hasHeader ? 2 : 1),
  // itemBuilder: _itemBuilder,
  // )

  Widget _build() {
    return RefreshIndicator(
      onRefresh: () => refresh(true),
      child: CustomScrollView(
        controller: widget.scrollController,
        slivers: [
          SliverList(
            delegate: SliverChildBuilderDelegate(
              _itemBuilder,
              childCount: state.items.length + (_hasHeader ? 2 : 1),
            ),
          )
        ],
      ),
    );
  }

  Widget _loadingMoreView() {
    if (widget.loadingBuilder != null) {
      return widget.loadingBuilder!(context);
    }
    return Container(
      padding: const EdgeInsets.all(16.0),
      alignment: Alignment.center,
      child: SizedBox(
        width: 20.0,
        height: 20.0,
        child: CircularProgressIndicator(strokeWidth: 2.0),
      ),
    );
  }

  Widget _buildEmptyView(context) {
    return Material(
      //color: Colors.transparent,
      child: InkWell(
        splashColor: Theme.of(context).secondaryHeaderColor,
        onTap: () => refresh(false),
        child: Center(
          child: Text("No data"),
        ),
      ),
    );
  }

  Widget _itemBuilder(BuildContext context, int index) {
    if (_hasHeader) {
      if (index == 0) {
        Widget header = widget.headerBuilder!(context, state.items);
        if (state.isLoading && state.isEmpty) {
          header = Column(
            children: <Widget>[
              header,
              _loadingMoreView(),
            ],
          );
        }
        return header;
      }
      --index;
    }
    if (index == state.items.length) {
      if (error != null) {
        Widget e;
        if (widget.loadMoreErrorViewBuilder != null) {
          e = widget.loadMoreErrorViewBuilder!(context, error!);
        } else {
          e = Text('Error, Click to retry!');
        }
        return Listener(
          child: Center(child: e),
          onPointerUp: (event) {
            setState(() {
              error = null;
              loadMore();
            });
          },
        );
      } else if (state.isNoMore || state.isLoading) {
        if (widget.noMoreViewBuilder != null) {
          return widget.noMoreViewBuilder!(context, state.items);
        } else {
          return Container(
            alignment: Alignment.center,
            padding: EdgeInsets.all(16.0),
            child: Text(
              "Total: ${state.items.length}",
              style: TextStyle(color: Colors.grey),
            ),
          );
        }
      } else {
        loadMore();
        return _loadingMoreView();
      }
    } else {
      var w = widget.itemBuilder(context, state.items, index);
      if (widget.separatorBuilder != null) {
        return Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            w,
            widget.separatorBuilder!(context, state.items, index),
          ],
        );
      }
      return w;
    }
  }
}
