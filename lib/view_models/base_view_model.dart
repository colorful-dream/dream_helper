import 'dart:async';

import 'package:dio/dio.dart';
import 'package:dream_helper/network_service/network.dart';
import 'package:dream_helper/view_models/view_model.dart';

typedef ValueConvert<T, M> = Future<M> Function<T>(T);

typedef ErrorCheck<T> = Error? Function<T>(T);

/// 接收到的数据进行处理

class BaseViewModel extends ViewModel {
  BaseViewModel({required Network network}) : super(network: network);

  /// Dio 请求获取到的数据 可能需要进行判断
  Future<M> request<T, M>(
    String url,
    ValueConvert<Response<T>, M> valueConvert, {
    ErrorCheck<Response<T>>? errorCheck,
    Map<String, dynamic>? queryParameters,
    Options? options,
    ProgressCallback? onSendProgress,
    ProgressCallback? onReceiveProgress,
  }) async {
    return network
        .request<T>(url,
            queryParameters: queryParameters,
            options: options,
            onSendProgress: onSendProgress,
            onReceiveProgress: onReceiveProgress)
        .then((value) {
      if (errorCheck != null) {
        Error? err = errorCheck(value);
        if (err != null) {
          throw err;
        }
      }
      return valueConvert(value.data);
    }).catchError((err) {
      throw err;
    });
  }

}
