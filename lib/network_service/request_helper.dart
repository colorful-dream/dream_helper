import 'request_status.dart';

class RequestHelper<T> {
  /// 数据源
  List<T> items = [];

  /// 当前页 默认0
  int currentPage = 0;

  /// 数据状态
  DataStatus dataStatus = DataStatus.none;

  /// 请求状态
  RequestStatus requestStatus = RequestStatus.none;

  /// 是否为空
  bool get isEmpty => dataStatus == DataStatus.empty;

  /// 没有更多
  bool get isNoMore => dataStatus == DataStatus.noMore;

  /// 是否在加载
  bool get isLoading => requestStatus == RequestStatus.loading;

  /// 错误
  dynamic error;
}
