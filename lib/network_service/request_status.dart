///
/// package: dream_helper
///
/// fileName: request_status.dart
/// 网络请求状态
///
/// author mario
/// created 2021/7/19

/// 网络请求状态
enum RequestStatus {
  /// 无状态
  none,

  /// 加载中
  loading,

  /// 出错
  error,

  /// 空闲 请求完成后进入空闲状态
  idle
}

enum DataStatus {
  /// 默认 用来展示第一次内容
  none,

  /// 正常
  normal,

  /// 空
  empty,

  /// 错误
  error,

  /// 没有更多
  noMore
}
