import 'package:dream_helper/dream_helper.dart';
import 'package:dream_helper/view_models/view_model.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyrefresh/easy_refresh.dart';

Widget widgetBuilder(
    WidgetBuilder? builder, BuildContext context, Widget widget) {
  return builder != null ? builder(context) : widget;
}

Widget? widgetNullableBuilder(WidgetBuilder? builder, BuildContext context) {
  return builder == null ? null : builder(context);
}

Header header(bool infiniteRefresh) {
  return ClassicalHeader(
      enableInfiniteRefresh: infiniteRefresh,
      refreshText: "下拉获取数据",
      refreshReadyText: "松开获取",
      refreshingText: "正在加载中...");
}

Footer footer() {
  return ClassicalFooter();
}

typedef FutureVoidCallBack = Future<void> Function();

class PlaceHolderWidget<T extends ViewModel> extends StatelessWidget {
  const PlaceHolderWidget({
    Key? key,
    required this.viewModel,
    this.error,
    this.empty,
    this.loading,
  }) : super(key: key);

  final T viewModel;

  /// 加载中的页面
  final WidgetBuilder? loading;

  /// 错误页
  final WidgetBuilder? error;

  /// 空白页
  final WidgetBuilder? empty;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Builder(
        builder: (context) {
          switch (viewModel.requestStatus) {
            case RequestStatus.loading:
              return widgetBuilder(
                loading,
                context,
                Center(
                  child: CupertinoActivityIndicator(),
                ),
              );
            case RequestStatus.error:
              return widgetBuilder(
                error,
                context,
                Center(
                  child: Icon(
                    Icons.error_outline,
                    color: Colors.red,
                  ),
                ),
              );
            case RequestStatus.idle:
              if (viewModel.dataStatus == DataStatus.empty) {
                return widgetBuilder(
                  empty,
                  context,
                  Center(
                    child: Text("数据为空"),
                  ),
                );
              } else {
                return Center(
                  child: Text("提示信息"),
                );
              }
            case RequestStatus.none:
              return Center(
                child: Text("提示信息"),
              );
          }
        },
      ),
    );
  }
}

class RefreshWidget<T> extends StatefulWidget {
  const RefreshWidget(
      {Key? key,
      required this.slivers,
      required this.easyRefreshController,
      this.firstRefreshWidget,
      this.refresh})
      : super(key: key);

  /// slivers
  final List<Widget> slivers;

  /// 下拉刷新
  final FutureVoidCallBack? refresh;

  /// controller
  final EasyRefreshController easyRefreshController;

  /// 首次进入
  final WidgetBuilder? firstRefreshWidget;

  @override
  _RefreshWidgetState createState() => _RefreshWidgetState<T>();
}

class _RefreshWidgetState<T> extends State<RefreshWidget<T>> {
  late ScrollController scrollController = ScrollController();

  @override
  void initState() {
    super.initState();
    scrollController.addListener(() {});
  }

  @override
  Widget build(BuildContext context) {
    // return EasyRefresh.custom(
    //   slivers: widget.slivers,
    //   scrollDirection: Axis.vertical,
    //   controller: widget.easyRefreshController,
    //   scrollController: scrollController,
    //   firstRefreshWidget:
    //       widgetNullableBuilder(widget.firstRefreshWidget, context),
    //   header: widget.refresh != null ? header(true) : null,
    //   footer: widget.load != null ? footer() : null,
    //   onRefresh: widget.refresh,
    //   onLoad: widget.load,
    // );
    return RefreshIndicator(
        child: CustomScrollView(
          slivers: widget.slivers,
          controller: scrollController,
        ),
        onRefresh: () async {
          await Future.delayed(Duration(milliseconds: 2000));
          //结束刷新
          return Future.value(true);
        });
  }
}
