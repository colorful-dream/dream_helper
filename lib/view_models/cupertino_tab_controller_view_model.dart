import 'package:flutter/cupertino.dart';

class CupertinoTabControllerViewModel extends ChangeNotifier {
  CupertinoTabController _controller;

  CupertinoTabControllerViewModel(this._controller);

  int get currentIndex => _controller.index;

  void changedIndex(int index) {
    _controller.index = index;
    notifyListeners();
  }
}
