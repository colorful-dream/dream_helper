import 'package:flutter/cupertino.dart' show Widget, WidgetBuilder, IndexedWidgetBuilder;

class WidgetItem {
  /// tabBarItem
  Widget item;

  /// builder for body
  WidgetBuilder widgetBuilder;

  /// construct
  WidgetItem(this.item, this.widgetBuilder);
}

class IndexedWidgetItem {
  /// tabBarItem
  Widget item;

  /// builder for body
  IndexedWidgetBuilder indexedWidgetBuilder;

  /// construct
  IndexedWidgetItem(this.item, this.indexedWidgetBuilder);
}