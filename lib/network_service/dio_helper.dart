import 'package:dio/dio.dart';

class DioHelper {

  /// 连接超时
  static const int connectTimeout = 3000;

  /// 链接超时
  static const int sendTimeout = 3000;

  /// 响应超时
  static const int receiveTimeout = 3000;

  /// static [_shared]
  static DioHelper _shared = DioHelper._internal();

  /// private named construct
  DioHelper._internal();

  /// factory
  factory DioHelper() => _shared;

  /// [shared]
  static DioHelper get shared => _shared;

  /// private [_dio]
  Dio _dio = Dio(
    BaseOptions(
      connectTimeout: DioHelper.connectTimeout,
      sendTimeout: DioHelper.connectTimeout,
      receiveTimeout: DioHelper.receiveTimeout,
    ),
  );

  /// getter [_dio]
  Dio get dio => _dio;

  /// 配置 [options]
  void optionsConfiguration(BaseOptions options) {
    _dio.options = options;
  }

  /// 添加 [interceptor]
  void interceptorsConfiguration(Interceptor interceptor) {
    _dio.interceptors.add(interceptor);
  }

  /// 添加 [interceptors]
  void interceptorsConfigurations(Interceptors interceptors) {
    _dio.interceptors.addAll(interceptors);
  }
}
