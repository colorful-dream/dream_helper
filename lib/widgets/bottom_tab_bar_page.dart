import 'package:dream_helper/dream_helper.dart';
import 'package:flutter/cupertino.dart';
import "package:flutter/material.dart"
    show
        BottomNavigationBar,
        BottomNavigationBarItem,
        BottomNavigationBarType,
        BuildContext,
        IndexedWidgetBuilder,
        Key,
        Scaffold,
        State,
        StatefulWidget,
        TabController,
        Widget;

import 'provider_widget.dart';

/// IndexPage
///
class BottomTabBarPage<T extends Widget> extends StatefulWidget {
  /// Constructor
  const BottomTabBarPage(
      {Key? key,
      this.initialIndex = 0,
      required this.indexedWidgetBuilder,
      required this.items,
      this.itemScrollable = false})
      : assert(items.length > 0),
        super(key: key);

  /// 初始索引
  final int initialIndex;

  /// 构建widget
  final IndexedWidgetBuilder indexedWidgetBuilder;

  /// bottom items
  final List<BottomNavigationBarItem> items;

  /// PageView 是否可以滑动
  final bool itemScrollable;

  @override
  _BottomTabBarPageState<T> createState() => _BottomTabBarPageState<T>();
}

class _BottomTabBarPageState<T extends Widget>
    extends State<BottomTabBarPage<T>> with TickerProviderStateMixin {
  /// 长度
  late int _length;

  @override
  void initState() {
    super.initState();
    _length = widget.items.length;
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return scaffold(context);
  }

  ProviderWidget cupertino(BuildContext context) {
    return ProviderWidget<CupertinoTabController>(
        viewModel: CupertinoTabController(initialIndex: widget.initialIndex),
        valueWidgetBuilder: (context, viewModel, child) {
          return cupertinoTabScaffold(context, viewModel, child);
        });
  }

  CupertinoTabScaffold cupertinoTabScaffold(
      BuildContext context, CupertinoTabController viewModel, Widget? child) {
    return CupertinoTabScaffold(
      controller: viewModel,
      tabBar: CupertinoTabBar(
        items: widget.items,
        onTap: (index) {
          viewModel.index = index;
        },
        currentIndex: viewModel.index,
      ),
      tabBuilder: widget.indexedWidgetBuilder,
    );
  }

  ProviderWidget scaffold(BuildContext context) {
    return ProviderWidget<TabController>(
        viewModel: TabController(
          vsync: this,
          initialIndex: widget.initialIndex,
          length: _length,
        ),
        valueWidgetBuilder: (context, viewModel, child) {
          return materialScaffold(context, viewModel, child);
        });
  }

  Scaffold materialScaffold(
      BuildContext context, TabController viewModel, Widget? child) {
    return Scaffold(
      bottomNavigationBar: BottomNavigationBar(
        items: widget.items,
        type: BottomNavigationBarType.fixed,
        selectedFontSize: 14.0,
        unselectedFontSize: 14.0,
        onTap: (index) {
          viewModel.index = index;
        },
        currentIndex: viewModel.index,
      ),
      body: widget.indexedWidgetBuilder(context, viewModel.index),
    );
  }
}
