import 'dart:async';

import 'package:dio/dio.dart';

/// 提供网络请求
abstract class Network {
  /// reason
  static const cancelReason = "用户取消";

  /// [dio] for making a request
  Dio dio;

  /// construct
  Network(this.dio);

  /// [_cancelToken]
  CancelToken _cancelToken = CancelToken();

  /// [cancelToken] getter
  CancelToken get cancelToken => _cancelToken;

  ///  cancel request
  void cancel({String reason = Network.cancelReason}) {
    this.cancelToken.cancel(reason);
  }

  /// 网络请求
  Future<Response<T>> request<T>(String url,
      {Map<String, dynamic>? queryParameters,
      Options? options,
      ProgressCallback? onSendProgress,
      ProgressCallback? onReceiveProgress}) async {
    return dio.request<T>(url,
        queryParameters: queryParameters,
        cancelToken: cancelToken,
        options: options,
        onSendProgress: onSendProgress,
        onReceiveProgress: onReceiveProgress);
  }

  Future<Response<T>> get<T>(String url,
      {Map<String, dynamic>? queryParameters,
      Options? options,
      ProgressCallback? onSendProgress,
      ProgressCallback? onReceiveProgress}) async {
    Options cu = options ?? Options();
    cu.method = "GET";
    return request(url,
        queryParameters: queryParameters,
        options: cu,
        onReceiveProgress: onReceiveProgress,
        onSendProgress: onSendProgress);
  }

  Future<Response<T>> post<T>(String url,
      {Map<String, dynamic>? queryParameters,
      Options? options,
      ProgressCallback? onSendProgress,
      ProgressCallback? onReceiveProgress}) async {
    Options cu = options ?? Options();
    cu.method = "POST";
    return request(url,
        queryParameters: queryParameters,
        options: cu,
        onReceiveProgress: onReceiveProgress,
        onSendProgress: onSendProgress);
  }
}
